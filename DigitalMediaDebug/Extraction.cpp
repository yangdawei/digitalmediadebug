#include "Extraction.h"
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

CExtraction::CExtraction(void) {
}

CExtraction::CExtraction(Mat mRgb) {
	img = mRgb;
}

CExtraction::~CExtraction(void) {
}

void CExtraction::CalculateBlob() {
	Mat src = img;
	if (src.empty()) {
		cout << "输入图片路径有误，请确认！" << endl;
		return;
	}
    
	Mat grayImg;
	cvtColor(src, grayImg, CV_BGRA2GRAY, 0);
    
	Mat binImage;
	//后期阈值会与另一个方式结合起来
	threshold(grayImg, binImage, 60, 255, CV_THRESH_BINARY);
	//如果是白色边界，则需要使用下述函数
	//bitwise_not(binImage,binImage);
    
	vector<vector<Point> > contours;
	findContours(binImage, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
	//drawContours(src, contours, -1, Scalar(255, 0, 0), 2);
	//imwrite("contours.jpg", src);
	int width = src.cols;
	int height = src.rows;
	vector<vector<Point> >::iterator it;
	for (it = contours.begin(); it != contours.end();) //删除过小的轮廓
    {
		Rect R = boundingRect(Mat(*it));
		if (R.width < width / 6 || R.height < height / 6)
			it = contours.erase(it);
		else
			++it;
	}
    
	for (it = contours.begin(); it != contours.end(); ++it) {
		Mat img(src.rows, src.cols, CV_8UC1);
		Scalar color(255, 255, 255);
		Mat binImage;
		threshold(img, img, 210, 255, CV_THRESH_BINARY);
		//bitwise_not(binImage,binImage);
		drawContours(img, contours, int(it-contours.begin()), color);
        
        
		//1.获取外界四边形
		Rect R = boundingRect(Mat(*it));
		//2.对区域进行填充
		Point tl = R.tl();
		Point br = R.br();
		for (int r = tl.y; r < br.y; ++r) {
			uchar* data = img.ptr<uchar>(r);
			int left = tl.x, right = br.x;
			while (data[left] != 255)
				++left;
			while (data[right] != 255)
				--right;
			for (int col=left; col <= right; ++col)
				data[col] = 255;
		}
		//3.Canny求边缘，再用hough求直线，从中选取直线
		Mat edgeImg;
		Canny(img, edgeImg, 100, 100, 3);
		vector<Vec4i> lines;
		//HoughLinesP(edgeImg,lines,1,CV_PI/180,40,20,30);
		HoughLinesP(edgeImg, lines, 1, CV_PI / 180, 40, 60, 30);
		int k = int(lines.size());
		//若线段数量k超过4条，则从中选出合适的4条，并将其分为上、下、左、右四类
		Vec4i edgeLines[4]; //对应上、下、左、右
		//对边界线初始化
		for (int i(0); i < 4; ++i) {
			for (int j(0); j < 4; ++j)
				edgeLines[i][j] = -1;
		}
		for (int i(0); i < k; ++i) {
			Point endPnt_1(lines[i][0], lines[i][1]);
			Point endPnt_2(lines[i][2], lines[i][3]);
			//1.判断是横线还是竖线
			bool isVertical;
			if (abs(endPnt_1.y - endPnt_2.y) >= abs(endPnt_1.x - endPnt_2.x))
				isVertical = true;
			else
				isVertical = false;
			//2.针对线段类型，判断是否是边界线
			if (!isVertical) //横线的情况
			{
				if (edgeLines[0][0] == -1) //还没有横线进来
                {
					for (int k(0); k < 2; ++k)
						for (int j(0); j < 4; ++j)
							edgeLines[k][j] = lines[i][j];
				} else {
					Point topPnt =
                    lines[i][1] > lines[i][3] ?
                    Point(lines[i][2], lines[i][3]) :
                    Point(lines[i][0], lines[i][1]);
					Point bottomPnt =
                    lines[i][1] > lines[i][3] ?
                    Point(lines[i][0], lines[i][1]) :
                    Point(lines[i][2], lines[i][3]);
					if (edgeLines[0][1] > topPnt.y) {
						edgeLines[0][0] = topPnt.x;
						edgeLines[0][1] = topPnt.y;
						edgeLines[0][2] = bottomPnt.x;
						edgeLines[0][3] = bottomPnt.y;
					} else if (edgeLines[1][3] < bottomPnt.y) {
						edgeLines[1][0] = topPnt.x;
						edgeLines[1][1] = topPnt.y;
						edgeLines[1][2] = bottomPnt.x;
						edgeLines[1][3] = bottomPnt.y;
					}
				}
			} else {
				if (edgeLines[2][0] == -1) //还没有竖线进来
                {
					for (int k(2); k < 4; ++k)
						for (int j(0); j < 4; ++j)
							edgeLines[k][j] = lines[i][j];
				} else {
					Point leftPnt =
                    lines[i][0] > lines[i][2] ?
                    Point(lines[i][2], lines[i][3]) :
                    Point(lines[i][0], lines[i][1]);
					Point rightPnt =
                    lines[i][0] > lines[i][2] ?
                    Point(lines[i][0], lines[i][1]) :
                    Point(lines[i][2], lines[i][3]);
					if (edgeLines[2][0] > leftPnt.x) {
						edgeLines[2][0] = leftPnt.x;
						edgeLines[2][1] = leftPnt.y;
						edgeLines[2][2] = rightPnt.x;
						edgeLines[2][3] = rightPnt.y;
					} else if (edgeLines[3][2] < rightPnt.x) {
						edgeLines[3][0] = leftPnt.x;
						edgeLines[3][1] = leftPnt.y;
						edgeLines[3][2] = rightPnt.x;
						edgeLines[3][3] = rightPnt.y;
					}
				}
			}
		}
		Point corner[4]; //QPoint corner[4];
		Point a[2], b[2]; //QPoint a[2],b[2];
		a[0] = Point(edgeLines[0][0], edgeLines[0][1]);
		a[1] = Point(edgeLines[0][2], edgeLines[0][3]);
		b[0] = Point(edgeLines[2][0], edgeLines[2][1]);
		b[1] = Point(edgeLines[2][2], edgeLines[2][3]);
		//line(src, a[0], a[1], Scalar(0, 0, 255), 3);
		//line(src, b[0], b[1], Scalar(0, 0, 255), 3);
		corner[0] = computeIntersect(a, b);
		a[0] = Point(edgeLines[0][0], edgeLines[0][1]);
		a[1] = Point(edgeLines[0][2], edgeLines[0][3]);
		b[0] = Point(edgeLines[3][0], edgeLines[3][1]);
		b[1] = Point(edgeLines[3][2], edgeLines[3][3]);
		//line(src,a[0],a[1],Scalar(0,0,255),3);
		//line(src, b[0], b[1], Scalar(0, 0, 255), 3);
		corner[1] = computeIntersect(a, b);
		a[0] = Point(edgeLines[1][0], edgeLines[1][1]);
		a[1] = Point(edgeLines[1][2], edgeLines[1][3]);
		b[0] = Point(edgeLines[3][0], edgeLines[3][1]);
		b[1] = Point(edgeLines[3][2], edgeLines[3][3]);
		//line(src, a[0], a[1], Scalar(0, 0, 255), 3);
		//imwrite("edgeLines.jpg", src);
		corner[2] = computeIntersect(a, b);
		a[0] = Point(edgeLines[1][0], edgeLines[1][1]);
		a[1] = Point(edgeLines[1][2], edgeLines[1][3]);
		b[0] = Point(edgeLines[2][0], edgeLines[2][1]);
		b[1] = Point(edgeLines[2][2], edgeLines[2][3]);
		corner[3] = computeIntersect(a, b);
		vector<Point> vertices; //vector<QPoint> vertices;
		for (int i(0); i < 4; ++i)
			vertices.push_back(corner[i]);
		vertexSet.push_back(vertices);
	}
}

void CExtraction::GetVertexSet(std::vector<polygon>* pPolygonSet) {
	pPolygonSet->clear();
	int polyNum = int(vertexSet.size());
	for (int i(0); i < polyNum; ++i) {
		polygon tempPoly;
		tempPoly.vertexNum = 4;
		for (int j(0); j < 4; ++j)
			tempPoly.vertex[j] = vertexSet[i][j];
		pPolygonSet->push_back(tempPoly);
	}
}

//QPoint CExtraction_Hough::computeIntersect( QPoint* a,QPoint* b )
Point CExtraction::computeIntersect(Point* a, Point* b) {
	//int x1 = a[0].x(),y1 = a[0].y(),x2 = a[1].x(),y2 = a[1].y();
	//int x3 = b[0].x(),y3 = b[0].y(),x4 = b[1].x(),y4 = b[1].y();
	int x1 = a[0].x, y1 = a[0].y, x2 = a[1].x, y2 = a[1].y;
	int x3 = b[0].x, y3 = b[0].y, x4 = b[1].x, y4 = b[1].y;
	int m = (x1 - x2) * (x3 - x4) * (y1 - y3);
	int k = (y3 - y4) * (x1 - x2);
	int l = (y1 - y2) * (x3 - x4);
	float d = float(k - l);
	if (!d)
		return Point(-1, -1); //return QPoint(-1,-1);
	Point pt; //QPoint pt;
	pt.x = (m + k * x3 - l * x1) / d; //pt.setX((m+k*x3-l*x1)/d);
	if (x1 == x2)
		pt.y = (pt.x - x3) * (y3 - y4) / (x3 - x4) + y3; //pt.setY((pt.x()-x3)*(y3-y4)/(x3-x4)+y3);
	else
		pt.y = (pt.x - x1) * (y1 - y2) / (x1 - x2) + y1; //pt.setY((pt.x()-x1)*(y1-y2)/(x1-x2)+y1);
	return pt;
}
