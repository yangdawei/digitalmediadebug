#ifndef EXTRACTION_HOUGH_H
#define EXTRACTION_HOUGH_H
#include "Type.h"
#include <vector>
#include <string>

class CExtraction
{
public:
	CExtraction(void);
	//CExtraction_Hough(QString path);
	CExtraction(Mat mRgb);
	~CExtraction(void);
	void CalculateBlob();
	void GetVertexSet( std::vector<polygon>* pPolygonSet);

private:
	//QPoint computeIntersect(QPoint* a,QPoint* b);
	cv::Point computeIntersect(cv::Point* a,cv::Point* b);
private:
	//QString imgPath;
	Mat img;
	//std::vector<std::vector<QPoint>> vertexSet; 
	std::vector<std::vector<cv::Point> > vertexSet;
};

#endif

