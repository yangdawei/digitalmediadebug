#ifndef TYPE_H
#define TYPE_H
//#include <QPoint>
#include <opencv2/opencv.hpp>
#include <vector>
using namespace cv;
using namespace std;

const int maxPointNum(10);//初步将点集的点数上限设置为10
struct polygon
{
	polygon(int verNum = 0)
        : vertexNum(verNum)
	{
        for (int i(0);i<maxPointNum;++i)
		{
			vertex[i].x = 0;
			vertex[i].y = 0;
			//vertex[i].setX(0);
			//vertex[i].setY(0);
		}
	}
	int vertexNum;
	cv::Point vertex[maxPointNum];
	//QPoint vertex[maxPointNum];
};

struct backupInfo
{
	std::vector<polygon> polygonSet;
	double scale;
	backupInfo(const std::vector<polygon>& curPolySet = std::vector<polygon>(),
               const double& curScale = 0):polygonSet(curPolySet),scale(curScale)
	{}
};

struct manualBackupInfo
{
	std::vector<polygon> polygonSet;
	double scale;
	polygon curPoly;
	manualBackupInfo(const std::vector<polygon>& curPolySet = std::vector<polygon>(),
                     double curScale = 0,const polygon& tempPoly = polygon()):
	polygonSet(curPolySet),scale(curScale),curPoly(tempPoly)
	{}
};

#endif // TYPE_H

