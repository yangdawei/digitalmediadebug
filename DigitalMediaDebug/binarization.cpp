//
//  binarization.cpp
//  DigitalMediaDebug
//
//  Created by Dawei Yang on 8/5/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#include "binarization.h"

using namespace cv;
using namespace std;

Mat sauvola(const Mat &_input, int winSize = 50, float k = 0.1f)
{
    int cols = _input.cols;
    int rows = _input.rows;
    Mat ret(_input.size(), CV_32FC1);
    Mat fInput;
    _input.convertTo(fInput, CV_32FC1);
    
    double maxStddev = 0.0;
    for (int row = 0; row < rows; row += winSize)
        for (int col = 0; col < cols; col += winSize)
        {
            int theight = winSize;
            int twidth = winSize;
            if (row + winSize > rows)
                theight = rows - row;
            if (col + winSize > cols)
                twidth = cols - col;
            
            Mat mmean;
            Mat mstddev;
            
            Mat roi = fInput.rowRange(row, row + theight)
            .colRange(col, col+ twidth);
            //NOTE: Possibly replace double with float on a x86 machine
            meanStdDev(roi, mmean, mstddev);
            double dstddev = mstddev.at<double>(0);
            if (maxStddev < dstddev)
                maxStddev = dstddev;
        }
    
    for (int row = 0; row < rows; row += winSize)
    {
        for (int col = 0; col < cols; col += winSize)
        {
            int theight = winSize;
            int twidth = winSize;
            if (row + winSize > rows)
                theight = rows - row;
            if (col + winSize > cols)
                twidth = cols - col;
            
            Mat mmean;
            Mat mstddev;
            
            Mat roi = fInput.rowRange(row, row + theight)
                            .colRange(col, col+ twidth);
            //NOTE: Possibly replace double with float on a x86 machine
            meanStdDev(roi, mmean, mstddev);
            double dmean = mmean.at<double>(0);
            double dstddev = mstddev.at<double>(0);
            printf("%.10f, %.10f\n", dmean, dstddev);
            
            // With mean and stddev within a small region
            // Calculate the local threshold
            int thresh = (int) (dmean * (1 + k * (dstddev / maxStddev - 1)));
            threshold(roi, ret.rowRange(row, row + theight)
                      .colRange(col, col + twidth), thresh, 255, CV_THRESH_BINARY);
        }
    }
    ret.convertTo(ret, CV_8UC1);
    return ret;
}

void histogram_adjustify(int start, int end)
{
    char buffer[4096];
    for (int i = start; i < end; i++)
    {
        sprintf(buffer, "out/picture_%d.jpg", i);
        Mat color = imread(buffer, CV_LOAD_IMAGE_COLOR);
        Mat mat;
        cvtColor(color, mat, CV_RGB2GRAY);
        Mat binarization = sauvola(mat);
        cvtColor(binarization, binarization, CV_GRAY2RGB);
//        imshow("test", binarization);
        const Scalar full(255, 255, 255);
        Mat invertColor = full - color;
//        imshow("invertColor", invertColor);
//        waitKey();
        
        Mat invertBin = (full - binarization);
//        imshow("invertBin", invertBin);
//        waitKey();
        
        Mat invertResult = (invertColor / 255).mul(invertBin);
        Mat result = full - invertResult;
//        imshow("result", result);
//        waitKey();
        sprintf(buffer, "result/picture_%d.jpg", i);
        imwrite(buffer, result);
    }
}
