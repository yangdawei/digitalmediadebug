//
//  binarization.h
//  DigitalMediaDebug
//
//  Created by Dawei Yang on 8/5/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#ifndef __DigitalMediaDebug__binarization__
#define __DigitalMediaDebug__binarization__

#include <stdio.h>
#include <opencv2/opencv.hpp>

void histogram_adjustify(int start, int end);

#endif /* defined(__DigitalMediaDebug__binarization__) */
