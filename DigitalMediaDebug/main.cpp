//
//  main.cpp
//  DigitalMediaDebug
//
//  Created by Dawei Yang on 7/18/14.
//  Copyright (c) 2014 Dawei Yang. All rights reserved.
//

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>

#include "Extraction.h"
#include "binarization.h"

using namespace cv;

void PerspectiveTransform(const Mat& img,const vector<polygon> &pol,int id)
{
	//FILE* fp2=fopen("/sdcard/transform/errlog2.txt","w");
    //下面是perspectivetransform
	CvPoint2D32f srcTri[4], dstTri[4]; //二维坐标下的点，类型为浮点
    //CvMat* rot_mat = cvCreateMat( 2, 3, CV_32FC1 );  //多通道矩阵
    CvMat* warp_mat = cvCreateMat( 3, 3, CV_32FC1 );
    IplImage *src, *dst;
    
    // if( argv == 2 && ( ( src = cvLoadImage( argc[1], 1 ) ) != 0 ) )
    //{
    IplImage srcimg = img;
    src = &srcimg;
    dst = cvCloneImage( src );  //制作图像的完整拷贝
    dst ->origin = src ->origin;
    /*
     int origin; 0 - 顶—左结构,
     1 - 底—左结构 (Windows bitmaps 风格)
     */
    cvZero( dst );  //清空数组
    
    
    //计算矩阵仿射变换
    for(int j(0);j<4;++j)
    {
        srcTri[j].x = pol[0].vertex[j].x;
		srcTri[j].y = pol[0].vertex[j].y;
    }
    
    
    //改变目标图像大小
    //分别是0,0 0,width  height,0 width,height
    dstTri[0].x = 0;
    dstTri[0].y = 0;
    dstTri[1].x = src -> width;
    dstTri[1].y = 0;
    dstTri[2].x = src -> width;
    dstTri[2].y = src -> height;
    dstTri[3].x = 0;
    dstTri[3].y = src -> height;
    
    cvGetPerspectiveTransform( srcTri, dstTri, warp_mat );  //由三对点计算仿射变换
    cvWarpPerspective( src, dst, warp_mat );  //对图像做仿射变换
    char filename[255];
    sprintf(filename,"out/picture_%d.jpg",id);
    cvSaveImage(filename,dst);
    //输出
    // cvNamedWindow( "Perspective Warp", 1 );
    // cvShowImage( "Perspective Warp", dst );  //最终是输出dst
    // cvWaitKey();
    // }
    cvReleaseImage( &dst );
    cvReleaseMat( &warp_mat );
    
}

vector<Point2f> vertexDetect(const vector<Point2f> &points)
{
    size_t i1 = 0, i2 = 0, i3 = 0, i4 = 0;
    int sum1 = INT_MIN, sum2 = INT_MIN, sum3 = INT_MIN, sum4 = INT_MIN;
    for (size_t idx = 0; idx < points.size(); idx++)
    {
        Point2f pt = points[idx];
        if (sum1 < -pt.x + pt.y)
        {
            sum1 = -pt.x + pt.y;
            i1 = idx;
        }
        if (sum2 < pt.x + pt.y)
        {
            sum2 = pt.x + pt.y;
            i2 = idx;
        }
        if (sum3 < pt.x - pt.y)
        {
            sum3 = pt.x - pt.y;
            i3 = idx;
        }
        if (sum4 < -pt.y - pt.x)
        {
            sum4 = -pt.y - pt.x;
            i4 = idx;
        }
    }
    vector<Point2f> ret;
    ret.push_back(points[i4]);
    ret.push_back(points[i3]);
    ret.push_back(points[i2]);
    ret.push_back(points[i1]);
    return ret;
}

int main(int argc, const char *argv[])
{
    char buffer[4096];
    Mat sample = imread("data_0/picture_1.jpg");
    VideoWriter writer("out/out.avi", CV_FOURCC('M', 'J', 'P', 'G'), 5, sample.size());
    for (int i = 1; i < 6; i++)
    {
        sprintf(buffer, "data_0/picture_%d.jpg", i);
        Mat rgb = imread(buffer);
        Mat gray;
        cvtColor(rgb, gray, CV_BGR2GRAY);
        
        CExtraction extractionImp(rgb);
        extractionImp.CalculateBlob();
        vector<polygon> m_PolygonSet;
        extractionImp.GetVertexSet(&m_PolygonSet);
        
        Mat harris, harris_norm;
        normalize(harris, harris_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
        
        vector<Point2f> corners;
        goodFeaturesToTrack(gray, corners, 8, 0.01, 5);
        corners = vertexDetect(corners);
        
        for (size_t idx = 0; idx < corners.size(); idx++)
            circle(rgb, corners[idx], 4, Scalar(255, 128, 0));
        
        for (int i = 0; i < 4; i++)
        {
            for (size_t i = 0; i < 4; i++)
                m_PolygonSet[0].vertex[i] = corners[i];
            line(rgb, m_PolygonSet[0].vertex[i], m_PolygonSet[0].vertex[(i+1)%4], Scalar(0, 0, 255));
        }
        
        sprintf(buffer, "out/detect_%d.jpg", i);
        imwrite(buffer, rgb);
        
        PerspectiveTransform(rgb, m_PolygonSet, i);
        sprintf(buffer, "out/picture_%d.jpg", i);
        Mat transformed = imread(buffer);
        writer << transformed;
    }
    writer.release();
    
    histogram_adjustify(1, 6);
    return 0;
}


